#!/bin/bash
COMPILER_NDK=/myth/android/xperia/kernel/android-ndk-r13b/toolchains/aarch64-linux-android-4.9/prebuilt/linux-x86_64/bin/aarch64-linux-android-
COMPILER_UBER=/myth/android/mi5/kernel/DespairFactor-aarch64-linux-android-7.x-0e4f85204099/bin/aarch64-linux-android-
COMPILER_LINARO=/myth/android/mi5/kernel/prebuilts/gcc/linux-x86/aarch64/aarch64-linux-android-linaro-4.9/bin/aarch64-linux-android-
COMPILER_SABER=/myth/android/mi5/kernel/Sabermod_aarch64-7.0/bin/aarch64-

# Set compiler to one above it not already set in env
export CROSS_COMPILE=${CROSS_COMPILE:-$COMPILER_SABER}

echo "Compiling with: $CROSS_COMPILE"

export ARCH=arm64
export SUBARCH=arm64

export KBUILD_BUILD_USER='corona'

rm -f arch/arm64/boot/Image*
rm -f arch/arm/boot/dts/qcom/*.dtb

if [[ ! $REBUILD ]]; then
make clean && make mrproper
make lineageos_gemini_defconfig
else
echo "Rebuilding"
fi

export KCFLAGS="-I. -Wno-unused-const-variable -Wno-misleading-indentation -Wno-bool-compare -Wno-return-local-addr -Wno-format-truncation -Wno-bool-operation \
                -Wno-duplicate-decl-specifier -Wno-memset-elt-size -Wno-int-in-bool-context -Wno-parentheses -Wno-switch-unreachable -Wno-format-overflow"
nice make -j3 | tee ./make.log

echo "checking for compiled kernel..."
if [ -f arch/arm64/boot/Image ]
then

cat arch/arm/boot/dts/qcom/a1-msm8996-v2-pmi8994-mtp.dtb arch/arm/boot/dts/qcom/a1-msm8996-v3-pmi8994-mtp.dtb arch/arm/boot/dts/qcom/a1-msm8996-v3.0-pmi8994-mtp.dtb arch/arm/boot/dts/qcom/a2-msm8996-v3-pmi8994-mtp.dtb arch/arm/boot/dts/qcom/a2-msm8996-v3.0-pmi8994-mtp.dtb > arch/arm/boot/dts/qcom/Gemini.dtb
cat arch/arm64/boot/Image.gz-dtb arch/arm/boot/dts/qcom/Gemini.dtb  > arch/arm64/boot/zImage

find ./ -name '*.ko' -exec ./scripts/sign-file sha512 signing_key.priv signing_key.x509 {} \;

./rebuild_zip.sh

echo "DONE"

else
echo "Image not created"
exit -1
fi
