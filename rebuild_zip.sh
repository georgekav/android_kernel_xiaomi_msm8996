#!/bin/bash

mkdir output

# abootimg -u anl_boot.img -k arch/arm64/boot/zImage
#cp anl_boot.img output/

KERNEL_NAME=LineageOS-gemini-safetynet-kernel
KERNEL_ZIP=$KERNEL_NAME.zip
KERNEL_DATE_ZIP=${KERNEL_NAME}_$(date +%Y%m%d_%H%M).zip

rm ${KERNEL_NAME}*.zip
cd AK2-Kernel
cp ../arch/arm64/boot/zImage ./
rm -rf ./modules/*
find ../ -name '*.ko' -exec cp {} ./modules \;
7za a -r ../$KERNEL_ZIP ./*
cd ..

cp $KERNEL_ZIP output/$KERNEL_DATE_ZIP

